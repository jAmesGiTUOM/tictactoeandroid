package com.example.jamesauskew.tictactoe;

/**
 * Created by james on 16-1-18.
 */
public class CheckWinning {

    private MainActivity game;
    public CheckWinning(MainActivity game)

    {
        this.game=game;
    }


    public  void UserWin()
    {

        if((game.getCounter()[0]==1)&&(game.getCounter()[1]==1)&&(game.getCounter()[2]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[3]==1)&&(game.getCounter()[4]==1)&&(game.getCounter()[5]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[6]==1)&&(game.getCounter()[7]==1)&&(game.getCounter()[8]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[0]==1)&&(game.getCounter()[3]==1)&&(game.getCounter()[6]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[1]==1)&&(game.getCounter()[4]==1)&&(game.getCounter()[7]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[2]==1)&&(game.getCounter()[5]==1)&&(game.getCounter()[8]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[0]==1)&&(game.getCounter()[4]==1)&&(game.getCounter()[8]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }
        if((game.getCounter()[2]==1)&&(game.getCounter()[4]==1)&&(game.getCounter()[6]==1)) {
            game.setGameOver(true);
            game.toastMessage.show();

        }

    }
    public  void aiWin()
    {

        if((game.getCounter()[0]==2)&&(game.getCounter()[1]==2)&&(game.getCounter()[2]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[3]==2)&&(game.getCounter()[4]==2)&&(game.getCounter()[5]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[6]==2)&&(game.getCounter()[7]==2)&&(game.getCounter()[8]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[0]==2)&&(game.getCounter()[3]==2)&&(game.getCounter()[6]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[1]==2)&&(game.getCounter()[4]==2)&&(game.getCounter()[7]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[2]==2)&&(game.getCounter()[5]==2)&&(game.getCounter()[8]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[0]==2)&&(game.getCounter()[4]==2)&&(game.getCounter()[8]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }
        if((game.getCounter()[2]==2)&&(game.getCounter()[4]==2)&&(game.getCounter()[6]==2)) {
            game.setGameOver(true);
            game.aiWinningMessage.show();

        }

    }

    public void checkDraw(){
        boolean finished=true;
        for (int i = 0; i < 9; i++) {
            if(game.getPressed()[i]==false)
            {
                finished=false;
                break;
            }
        }
        if(finished==true&&game.isGameOver()==false)
        {
            game.setGameOver(true);
            game.drawMessage.show();

        }

    }
}
