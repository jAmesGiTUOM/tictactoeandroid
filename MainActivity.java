package com.example.jamesauskew.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button[] myButtonCollection=new Button[10];
    private int[] counter=new int[10];
    private Boolean[] pressed=new Boolean[10];
    private boolean gameOver;
    private Button restartButton;
    private Boolean[] defended=new Boolean[10];
    private myOwnListener[] listeners=new myOwnListener[10];
    private CheckWinning checkWin;
    private AiControl ai;

    protected Toast toastMessage;
    protected Toast aiWinningMessage;
    protected Toast drawMessage;

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize the messages
        toastMessage = Toast.makeText(this, "你赢了！", Toast.LENGTH_LONG);
        aiWinningMessage=Toast.makeText(this,"电脑赢了!",Toast.LENGTH_LONG);
        drawMessage=Toast.makeText(this,"平局",Toast.LENGTH_LONG);

        //associate the buttons
        myButtonCollection[0]=(Button)findViewById(R.id.button);
        myButtonCollection[1]=(Button)findViewById(R.id.button2);
        myButtonCollection[2]=(Button)findViewById(R.id.button3);
        myButtonCollection[3]=(Button)findViewById(R.id.button4);
        myButtonCollection[4]=(Button)findViewById(R.id.button5);
        myButtonCollection[5]=(Button)findViewById(R.id.button6);
        myButtonCollection[6]=(Button)findViewById(R.id.button7);
        myButtonCollection[7]=(Button)findViewById(R.id.button8);
        myButtonCollection[8]=(Button)findViewById(R.id.button9);

        this.restartButton=(Button)findViewById(R.id.restartButton);

        //assign the system control class
        checkWin=new CheckWinning(this);
        ai=new AiControl(this);


        //initialize the game,assign the button with the listeners
        for(int i=0;i<9;i++)
        {
            pressed[i]=false;
            defended[i]=false;
            listeners[i]=new myOwnListener(i,this);
            myButtonCollection[i].setOnClickListener(listeners[i]);

        }

        //set the restart listener
        View.OnClickListener myRestartListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int i=0;
                while(i<9)
                {
                    pressed[i]=false;
                    defended[i]=false;
                    myButtonCollection[i].setText(" ");
                    counter[i]=0;
                    i++;
                }
                gameOver=false;

            }
        } ;

        restartButton.setOnClickListener(myRestartListener);

    }


    //getters and setters
    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public Boolean[] getPressed() {
        return pressed;
    }


    public Button[] getMyButtonCollection() {
        return myButtonCollection;
    }



    public int[] getCounter() {
        return counter;
    }


    public Boolean[] getDefended() {
        return defended;
    }


    public CheckWinning getCheckWin() {
        return checkWin;
    }

    public AiControl getAi() {
        return ai;
    }
}