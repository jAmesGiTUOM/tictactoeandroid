package com.example.jamesauskew.tictactoe;

/**
 * Created by james on 16-1-19.
 */
public class AiControl {

    private MainActivity game;

    public AiControl(MainActivity game)
    {
        this.game=game;
    }
    public void aiReaction() {
        int i = 0;

        if (game.getPressed()[4] == false) {
            game.getMyButtonCollection()[4].setText("O");
            game.getPressed()[4] = true;
            game.getCounter()[4]=2;
            game.getCheckWin().aiWin();
        }
        else if(((game.getCounter()[1]==1&&game.getCounter()[2]==1)||(game.getCounter()[8]==1&&game.getCounter()[4]==1)||(game.getCounter()[3]==1&&game.getCounter()[6]==1))&&(!game.getDefended()[0]))
        {
            if(game.getPressed()[0]==false) {
                game.getMyButtonCollection()[0].setText("O");
                game.getPressed()[0] = true;
                game.getCounter()[0] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[0]=true;
            }
            else{
                autoAiReaction();
            }
        }
        else if(((game.getCounter()[0]==1&&game.getCounter()[1]==1)||(game.getCounter()[4]==1&&game.getCounter()[6]==1)||(game.getCounter()[5]==1&&game.getCounter()[8]==1))&&(!game.getDefended()[2]))
        {
            if(game.getPressed()[2]==false) {
                game.getMyButtonCollection()[2].setText("O");
                game.getPressed()[2] = true;
                game.getCounter()[2] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[2]=true;
            }
            else{
                autoAiReaction();
            }
        }
        else if(((game.getCounter()[0]==1&&game.getCounter()[3]==1)||(game.getCounter()[2]==1&&game.getCounter()[4]==1)||(game.getCounter()[7]==1&&game.getCounter()[8]==1))&&(!game.getDefended()[6]))
        {
            if(game.getPressed()[6]==false) {
                game.getMyButtonCollection()[6].setText("O");
                game.getPressed()[6] = true;
                game.getCounter()[6] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[6]=true;
            }
            else{
                autoAiReaction();
            }
        }
        else if(((game.getCounter()[0]==1&&game.getCounter()[4]==1)||(game.getCounter()[2]==1&&game.getCounter()[5]==1)||(game.getCounter()[7]==1&&game.getCounter()[6]==1))&&(!game.getDefended()[8]))
        {
            if(game.getPressed()[8]==false) {
                game.getMyButtonCollection()[8].setText("O");
                game.getPressed()[8] = true;
                game.getCounter()[8] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[8]=true;
            }
            else{
                autoAiReaction();
            }
        }
        else if(((game.getCounter()[1]==1&&game.getCounter()[4]==1)||(game.getCounter()[6]==1&&game.getCounter()[8]==1))&&(!game.getDefended()[7]))
        {
            if(game.getPressed()[7]==false) {
                game.getMyButtonCollection()[7].setText("O");
                game.getPressed()[7] = true;
                game.getCounter()[7] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[7]=true;
            }
            else{
                autoAiReaction();
            }
        }
        else if(((game.getCounter()[0]==1&&game.getCounter()[6]==1)||(game.getCounter()[4]==1&&game.getCounter()[5]==1))&&(!game.getDefended()[3]))
        {
            if(game.getPressed()[3]==false) {
                game.getMyButtonCollection()[3].setText("O");
                game.getPressed()[3] = true;
                game.getCounter()[3] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[3]=true;
            }
            else{
                autoAiReaction();
            }
        }
        else if(((game.getCounter()[3]==1&&game.getCounter()[4]==1)||(game.getCounter()[2]==1&&game.getCounter()[8]==1))&&(!game.getDefended()[5]))
        {
            if(game.getPressed()[5]==false) {
                game.getMyButtonCollection()[5].setText("O");
                game.getPressed()[5] = true;
                game.getCounter()[5] = 2;
                game.getCheckWin().aiWin();
                game.getDefended()[5]=true;
            }
            else{
                autoAiReaction();
            }
        }

        else {
            autoAiReaction();
        }
    }

    public void autoAiReaction()
    {
        int i=0;

        while (game.getPressed()[i] == true && (i < 8)) {

            i++;
        }
        if(game.getPressed()[i]==false) {
            game.getMyButtonCollection()[i].setText("O");
            game.getPressed()[i] = true;
            game.getCounter()[i] = 2;
            game.getCheckWin().aiWin();
        }
    }
}
