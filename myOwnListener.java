package com.example.jamesauskew.tictactoe;

import android.view.View;

/**
 * Created by james on 16-1-18.
 */
public class myOwnListener implements View.OnClickListener {

    private int listenerNum;
    private MainActivity game;

    public myOwnListener(int num,MainActivity game)
    {
        listenerNum=num;
        this.game=game;

    }


    public void onClick(View v) {

        if((!game.isGameOver())&&(!game.getPressed()[listenerNum])){
            game.getMyButtonCollection()[listenerNum].setText("X");
            game.getCounter()[listenerNum]=1;
            game.getPressed()[listenerNum]=true;
            game.getCheckWin().UserWin();
            game.getCheckWin().checkDraw();

            if(!game.isGameOver())
            {
                game.getAi().aiReaction();

            }


        }


    }

}
